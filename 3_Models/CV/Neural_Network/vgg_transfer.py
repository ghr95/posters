#
# packages 2 install
#

# ---------------------------------------------------------------------
# Load util
import matplotlib.pyplot as plt
import os

import numpy as np
import glob

from keras.models import Sequential, Model
from keras import optimizers
from keras.layers import Dense, Activation, Conv2D, MaxPool2D, Flatten, BatchNormalization, Dropout
from keras.preprocessing.image import ImageDataGenerator
from keras import applications

img_height = img_width = 224
channels = 3
if (channels == 1):
    color_mode_ = "grayscale"
else:
    color_mode_ = "rgb"

WORKING_DIR = os.getcwd()
WORKING_DIR = os.path.dirname(os.path.dirname(os.path.dirname(WORKING_DIR)))
print(os.path.join(WORKING_DIR, "_Data/splits/Comedy"))
dataset_folder_path = os.path.join(WORKING_DIR, "_Data/splits/Comedy")

train_folder = dataset_folder_path + '/train'
test_folder = dataset_folder_path + '/test'

test_files = glob.glob(test_folder + '/**/*.jpg')
train_files = glob.glob(train_folder + '/**/*.jpg')

train_examples = len(train_files)
test_examples = len(test_files)
print("Number of train examples: ", train_examples)
print("Number of test examples: ", test_examples)

# ---------------------------------------------------------------------


from keras.preprocessing.image import ImageDataGenerator

from keras.applications import InceptionResNetV2
from keras.applications import VGG16
from keras.applications import VGG19


# Load the VGG model
vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(img_width, img_height, channels))

# Freeze some layers
for layer in vgg_conv.layers[:-2]:
    layer.trainable = False


# Check the trainable status of the individual layers
for layer in vgg_conv.layers:
    print(layer, layer.trainable)

from keras import models
from keras import layers
from keras import optimizers

# Create the model
model = models.Sequential()

# Add the vgg convolutional base model
model.add(vgg_conv)

# Add new layers
model.add(Flatten())
model.add(Dense(2, activation='softmax'))

# Show a summary of the model. Check the number of trainable parameters
model.summary()

train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

validation_datagen = ImageDataGenerator(rescale=1. / 255)

# Change the batchsize according to your system RAM
train_batchsize = 100
val_batchsize = 10


train_generator = train_datagen.flow_from_directory(
    train_folder,
    target_size=(img_height, img_width),
    batch_size=train_batchsize,
    class_mode='categorical'
)

print('****************')
for cls, idx in train_generator.class_indices.items():
    print('Class #{} = {}'.format(idx, cls))
print('****************')


# Compile the model
adam = optimizers.Adam()
model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

validation_generator = validation_datagen.flow_from_directory(
    test_folder,
    target_size=(img_height, img_width),
    batch_size=val_batchsize,
    class_mode='categorical',
    shuffle=False)

# Train the model
history = model.fit_generator(
    train_generator,
    steps_per_epoch=train_generator.samples / train_generator.batch_size,
    epochs=2,
    validation_data=validation_generator,
    validation_steps=validation_generator.samples / validation_generator.batch_size,
    verbose=1)

# ---------------------------------------------------------------------
# test your accuracy
# ---------------------------------------------------------------------

# YOUR CODE
acc = model.evaluate_generator(validation_generator, test_examples // val_batchsize)
print(acc)

# WORKING_DIR = os.getcwd()
# WORKING_DIR = os.path.dirname(os.path.dirname(os.path.dirname(WORKING_DIR)))
# print(os.path.join(WORKING_DIR, "_Data/splits/Comedy"))
# dataset_folder_path = os.path.join(WORKING_DIR, "_Data/splits/Comedy")

model.save('comedy_vgg_30_4_10.h5')