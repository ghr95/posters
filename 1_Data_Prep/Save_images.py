"""Using the links to the movie posters in a csv, downloads all the images and puts them in a folder
Author: Elliot Weissberg"""
import pandas as pd
import urllib.request
import os
import urllib.error

no_picture_index = []


def main():
    df = pd.read_csv('MovieGenre.csv', encoding="ISO-8859-1")
    df = df.dropna()

    for index, row in df.iterrows():
        get_picture(row.Poster, index, row.imdbId)

        if index % 100 == 0:
            print(index)

    df = df.drop(index=no_picture_index)
    df.to_csv('Movie_genre_only_with_poster.CSV')


def get_picture(url, index, imdb_id):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    dir_path_folder = os.path.join(dir_path, "images")
    dir_file = os.path.join(dir_path_folder, str(imdb_id))

    if os.path.isdir(dir_path_folder) is False:
        os.makedirs(dir_path_folder)
    if os.path.isfile(dir_file) is False:
        try:
            urllib.request.urlretrieve(url, dir_file)
        except urllib.error.HTTPError:
            no_picture_index.append(index)


if __name__ == '__main__':
    main()

