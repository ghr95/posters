"""Program processes the csv taken from https://www.kaggle.com/neha1703/movie-genre-from-its-poster#MovieGenre.csv
for our purposes (turn genres into one-hot, drops columns, create title and year columns from title)
Author: Elliot Weissberg"""

import pandas as pd

file = 'Movie_genre_only_with_poster.csv'

df = pd.read_csv(file, encoding="ISO-8859-1")
df_copy = df.copy()

# Remove unwanted columns
df_copy = df_copy.drop(['Imdb Link', 'IMDB Score', 'Poster'], axis=1)

# Convert title column to title and year
df['year'] = 0
for index, _ in df_copy.iterrows():
    df_copy.loc[index, 'year'] = df_copy.loc[index, 'Title'][-5:-1]
    df_copy.loc[index, 'Title'] = df_copy.loc[index, 'Title'][0:-7]

# Create a list of all the genres present in the dataset
genrelist = list()

for index, _ in df_copy.iterrows():
    genres = str(df_copy.loc[index, "Genre"])
    genres = genres.split("|")
    genrelist.extend(genres)

unique_genres = list(set(genrelist))

# Create a separate column for each genre
for genre in unique_genres:
    df_copy[genre] = 0

# Do the one hot encoding of the genres
for index, _ in df_copy.iterrows():
    genres = str(df_copy.loc[index, "Genre"])
    genres = genres.split("|")
    for genre in genres:
        df_copy.loc[index, genre] = 1

# Drop the genre column
df_copy = df_copy.drop(['Genre'], axis=1)

# Drop year column that don't only contain a number (this happens with tv-shows due to the way the data was split
indexes_to_drop = list()
print(df_copy.info())
for idx, row in df_copy.iterrows():
    if row.year.isdigit() is False:
        print(idx)
        indexes_to_drop.append(idx)

df_copy = df_copy.drop(index=indexes_to_drop)
df_copy.year = df_copy.year.astype(int)

df_copy.to_csv('movie_genres_for_EDA.CSV', index=False)
