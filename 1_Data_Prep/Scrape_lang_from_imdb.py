"""Scraping languages and countries from an imdb page using the imdbid from the csv
BY: Elliot Weissberg"""
import requests
from bs4 import BeautifulSoup


def pad_with_zeros(imdbid):
    padded_len = 7
    id_len = len(str(imdbid))
    num_of_zeros = padded_len-id_len
    return '0' * num_of_zeros + str(imdbid)


def get_lang_and_country(imdbid):
    """Scrapes the imdb pages of the provided imdbids, and returns two lists: the movies' countries and languages """

    imdbid_padded = pad_with_zeros(imdbid)
    url = 'https://www.imdb.com/title/tt' + imdbid_padded + '/'

    try:
        webpage = requests.get(url)
        soup = BeautifulSoup(webpage.content, 'html.parser')

        title_details = soup.find('div', class_='article', id='titleDetails')

        text_blocks = title_details.find_all("div", {"class": "txt-block"})
        for text_block in text_blocks:
            detail = text_block.get_text().split()

            if detail[0] == 'Country:':
                countries = detail[1::2]

            elif detail[0] == 'Language:':
                languages = detail[1::2]

        print('scraped:', imdbid)

        return countries, languages

    except:

        print("Did not scrape:", imdbid)
        return None

# To test the code
# countries, languages = get_lang_and_country(192)
# print(countries)
# print(languages)

