"""Scraping languages and countries from an imdb page using the imdbid from the csv
BY: Elliot Weissberg"""
import requests
from bs4 import BeautifulSoup
import pandas as pd


def get_lang_and_country_r(imdbid):
    """Scrapes the imdb page of the provided imdbid, and returns the movie's countries and languages """
    url = 'https://www.imdb.com/title/tt0' + str(imdbid) + '/'
    try:
        webpage = requests.get(url)

        soup = BeautifulSoup(webpage.content, 'html.parser')

        title_details = soup.find('div', class_='article', id='titleDetails')
        # country = title_details.find('h4', class_='Country')
        # country = [atag.get_text() for atag in title_details.find_all("div", {"class": "txt-block"})[1]]
        countries_dirty = title_details.find_all("div", {"class": "txt-block"})[1].get_text().split()
        countries_clean = countries_dirty[1::2]

        languages_dirty = title_details.find_all("div", {"class": "txt-block"})[2].get_text().split()
        languages_clean = languages_dirty[1::2]
        print("Scraped:" + str(imdbid))

        return countries_clean, languages_clean

    except:
        print("Did not scrape:" + str(imdbid))

        return None

# get_lang_and_country(254199)